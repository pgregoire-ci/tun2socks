# tun2socks binary and ansible role

[![pipeline status](https://gitlab.com/pgregoire-ci/tun2socks/badges/main/pipeline.svg)](https://gitlab.com/pgregoire-ci/tun2socks/-/commits/main)

Provides a _nothing up my sleeve_`tun2socks` binary and an Ansible role
composed of a wrapper script to easily manage interfaces/tunnels
(`badtun`).

The `tun2socks` program is part of the `badvpn` project:
https://github.com/ambrop72/badvpn


## Binary

https://gitlab.com/pgregoire-ci/tun2socks/-/jobs/artifacts/main/raw/tun2socks?job=build


## Ansible

```
git submodule init
git submodule add https://gitlab.com/pgregoire-ci/tun2socks roles/tun2socks
```
